"""
Run an Austrian traffic light simulation on the Raspberry Pi Pico.
"""

from machine import Pin
from trafficlight_controller import TrafficLightController

GPIO_RED = 15
GPIO_YELLOW = 14
GPIO_GREEN = 13

LED_RED = Pin(GPIO_RED, Pin.OUT)
LED_YELLOW = Pin(GPIO_YELLOW, Pin.OUT)
LED_GREEN = Pin(GPIO_GREEN, Pin.OUT)

CONTROLLER = TrafficLightController(LED_GREEN, LED_YELLOW, LED_RED)
CONTROLLER.start()
