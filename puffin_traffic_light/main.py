"""
Run an Austrian traffic light simulation on the Raspberry Pi Pico.
"""

from machine import Pin

from trafficlight_controller import TrafficLightControllerWithButton

GPIO_BUZZER = 12
GPIO_RED = 15
GPIO_YELLOW = 14
GPIO_GREEN = 13
GPIO_BUTTON = 16
GPIO_PEDESTRIAN_GREEN = 25


BUZZER = Pin(GPIO_BUZZER, Pin.OUT)
LED_RED = Pin(GPIO_RED, Pin.OUT)
LED_YELLOW = Pin(GPIO_YELLOW, Pin.OUT)
LED_GREEN = Pin(GPIO_GREEN, Pin.OUT)
LED_PEDESTRIAN = Pin(GPIO_PEDESTRIAN_GREEN, Pin.OUT)
BUTTON = Pin(GPIO_BUTTON, Pin.IN, Pin.PULL_DOWN)

CONTROLLER = TrafficLightControllerWithButton(
    LED_GREEN, LED_YELLOW, LED_RED,
    BUTTON, BUZZER, LED_PEDESTRIAN)
CONTROLLER.start()

