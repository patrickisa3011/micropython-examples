"""
Traffic light controller simulating an Austrian traffic light.
"""

import _thread
from utime import sleep


class TrafficLightControllerWithButton:
    """
    Traffic light controller simulating an Austrian traffic light.

    When starting, the red light is on.
    red -> red + yellow -> green -> green blinks 4 times -> yellow -> RESTART
    """
    BLINK_DURATION = 0.5
    SHORT_DURATION = 2
    LONG_DURATION = 5
    BUZZER_DURATION = 0.2

    def __init__(self, green, yellow, red, button, buzzer, pedestrian_light):
        """
        Initialize Controller with three lights.

        Each light must have an API offering `on()` and `off()`.
        """
        self.green = green
        self.yellow = yellow
        self.red = red
        self.button = button
        self.buzzer = buzzer
        self.pedestrian_light = pedestrian_light
        self.__button_pressed = False

    def start(self):
        """
        Start an infinite loop through the controller's states.
        """
        _thread.start_new_thread(self.__button_reader_thread, ())
        while True:
            if self.__button_pressed:
                # TODO: refactor as state
                self.green.off()
                self.yellow.off()
                self.red.on()
                self.pedestrian_light.on()
                for _ in range(10):
                    self.buzzer.on()
                    sleep(self.BUZZER_DURATION)
                    self.buzzer.off()
                    sleep(self.BUZZER_DURATION)
                self.pedestrian_light.off()
                self.__button_pressed = False
            self.state_red()
            self.state_yellow_red()
            self.state_green()
            self.state_blinking()
            self.state_yellow()

    def __button_reader_thread(self):
        while True:
            if self.button.value() == 1:
                self.__button_pressed = True
            sleep(0.01)

    def state_red(self):
        self.green.off()
        self.yellow.off()
        self.red.on()
        sleep(self.LONG_DURATION)

    def state_yellow_red(self):
        self.green.off()
        self.red.on()
        self.yellow.on()
        sleep(self.SHORT_DURATION)

    def state_green(self):
        self.green.on()
        self.yellow.off()
        self.red.off()
        sleep(self.LONG_DURATION)

    def state_blinking(self):
        self.green.on()
        self.yellow.off()
        self.red.off()
        for _ in range(8):
            self.green.toggle()
            sleep(self.BLINK_DURATION)

    def state_yellow(self):
        self.green.off()
        self.yellow.on()
        self.red.off()
        sleep(self.SHORT_DURATION)
